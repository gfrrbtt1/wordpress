<div class="wrap">
    <h1>Novo Plugin com Menu de Exemplo</h1><br><br>
    <form action="options.php" method="POST">
        <?php
        settings_fields('configs-new-plugin-menu');
        do_settings_sections('configs-new-plugin-menu');
        ?>
        <label for="url-auth">URL Authentication</label>
        <input type="text" id="url-auth" name="url-auth" value="<?php echo get_option('url-auth'); ?>"><br><br>
        <label for="url-endpoint">URL End Point 1</label>
        <input type="text" id="url-endpoint" name="url-endpoint" value="<?php echo get_option('url-endpoint'); ?>"><br><br>
        <label for="url-token">URL Token</label>
        <input type="text" id="url-token" name="url-token" value="<?php echo get_option('url-token'); ?>"><br><br>
        <?php
        submit_button();
        ?>
    </form>
</div>