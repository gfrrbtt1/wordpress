<?php
/*
* Plugin Name: Novo Menu do Admin
* Plugin URI: https://sp.senac.br
* Description: Novo Menu do Admin
* Version: 0.0.1
* Author: Guilherme Batista
* Author URI: https://sp.senac.br
* License: CC BY
*/

add_action('admin_init', 'configs_new_plugin_menu');

function configs_new_plugin_menu(){
    register_setting('configs-new-plugin-menu', 'url-auth');
    register_setting('configs-new-plugin-menu', 'url-endpoint');
    register_setting('configs-new-plugin-menu', 'url-token');
}

add_action('admin_menu', 'gera_new_item_no_menu');

function gera_new_item_no_menu(){
    //Para criar um novo item no menu
    //add_menu_page('Configurações do Plugin Menu', 'Config Plugin Menu', 'administrator', 'config-plugin-menu', 'abre_config_plugin_menu');

    //Para criar um novo subitem em um item do menu
    add_submenu_page('options-general.php', 'Configurações do Novo Plugin Menu', 'Config Novo Plugin Menu', 'administrator', 'config-new-plugin-menu', 'abre_config_new_plugin_menu', 2);
}

function abre_config_new_plugin_menu(){
    require 'novo_menu_configs_view.php';
}