<?php
/*
* Plugin Name: Exemplo em POO
* Plugin URI: https://sp.senac.br
* Description: Exemplo de como criar um plug-in com programação orientada a objetos
* Version: 0.0.1
* Author: Guilherme Batista
* Author URI: https://sp.senac.br
* License: CC BY
*/

//Evita que o código seja executado se for chamado diretamente 
if (!defined ('WPINC')){
    die;
}

class PluginPoo {

    function __construct() {
        add_action('admin_init', array( $this, 'configs_plugin_menu'));
        add_action('admin_menu', array( $this, 'gera_item_no_menu'));
    }

    function configs_plugin_menu(){
        register_setting('configs-plugin-poo', 'url-api-auth-poo');
        register_setting('configs-plugin-poo', 'url-api-endpoint1-poo');
        register_setting('configs-plugin-poo', 'url-api-token-poo');
    }

    function gera_item_no_menu(){
        //Para criar um novo item no menu
        add_menu_page('Configurações do Plugin POO', 'Config Plugin POO', 'administrator', 'config-plugin-poo', array( $this, 'abre_config_plugin_menu'), 'dashicons-airplane');

        //Para criar um novo subitem em um item do menu
        //add_submenu_page('options-general.php', 'Configurações do Plugin Menu', 'Config Plugin Menu', 'administrator', 'config-plugin-menu', 'abre_config_plugin_menu', 2);
    }

    function abre_config_plugin_menu(){
        require 'plugin_poo_frontend.php';
    }

}

new PluginPoo;