<div class="wrap">
    <h1>Configurações de Plugin Exemplo com Menu</h1><br><br>
    <form action="options.php" method="POST">
        <?php
        settings_fields('configs-plugin-poo');
        do_settings_sections('configs-plugin-poo');
        ?>
        <label for="url-api-auth">URL Authentication</label>
        <input type="text" id="url-api-auth" name="url-api-auth-poo" value="<?php echo get_option('url-api-auth-poo'); ?>"><br><br>
        <label for="url-api-endpoint1">URL End Point 1</label>
        <input type="text" id="url-api-endpoint1" name="url-api-endpoint1-poo" value="<?php echo get_option('url-api-endpoint1-poo'); ?>"><br><br>
        <label for="url-api-token">URL Token</label>
        <input type="text" id="url-api-token" name="url-api-token-poo" value="<?php echo get_option('url-api-token-poo'); ?>"><br><br>
        <?php
        submit_button();
        ?>
    </form>
</div>