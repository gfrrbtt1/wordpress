<?php
/*
* Plugin Name: Olá Mundo, meu primeiro plug-in
* Plugin URI: https://sp.senac.br
* Description: Um plug-in massa de Hello Word
* Version: 0.0.1
* Author: Guilherme Batista
* Author URI: https://guilherme.com.br
* License: CC BY Gui
*/

// INICIO Exemplo de como utilizar um filter hook
add_filter('login_errors', 'nova_mensagem_de_erro');

function nova_mensagem_de_erro() {
    return 'Credenciais inválidas!';
}
// FIM Exemplo de como utilizar um filter hook

// Exemplo de como utilizar um action hook
add_action('wp_head', 'coloca_comentario');

function coloca_comentario(){
    if (is_single()){
        echo '<meta property="og:title" content="'.get_the_title().'">' . "\n";
        echo '<meta property="og:site_name" content="'.wp_title('', false).'">' . "\n";
        echo '<meta property="og:url" content="'.get_permalink(get_the_ID()).'">' . "\n";
    }
}
// FIM Exemplo de como utilizar um action hook
// Commit 19/10/2020