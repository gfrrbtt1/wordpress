<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wordpress' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'aLb-VxfF3.x_n9u>Ko#Q8:.z_!uW2K.HT{HIs[[s*)7x $2eB.Wh+HuAk#y.]C}z' );
define( 'SECURE_AUTH_KEY',  'i]1Wo+tXdC&I+=J2bTeB{:%&^9y>[oUR tBGJ}2X%K-xsam=DC>9/j^5m0D%:#D[' );
define( 'LOGGED_IN_KEY',    'M3[|Ga61:H;;;!d{84;!6CSjOLpk2vfUPNkcjYBie4r#yF#oUx6Zu)<psA;-*0B{' );
define( 'NONCE_KEY',        '~WkO5Wfi%b<jem!GFS$)5sj0[4TO3$%fh[pO-Rb#>cQ_HCMNrI,S!HmQ+SK!ppH*' );
define( 'AUTH_SALT',        'irHD@e)z4SX9b?3|Ivf0X})q2iJY=:iq k9URc*BU]PaymVC 6Kf{T(y]|cvje)f' );
define( 'SECURE_AUTH_SALT', 'J4>qD*g9a3gl[;|D!G~PPvDNF{o&RJ|}R>u5LdE-c`O:~)ZN|*G)P(weXz0=7c=x' );
define( 'LOGGED_IN_SALT',   'OW_VjCXJfjBGKo}5KEn+7ZPWbR|`2+h^fpT5=*YEjbZ_iU}r/V9mD~p2^FtR:/u_' );
define( 'NONCE_SALT',       'DnI6aiy)mjqYe,ilWQ2wTR~XUCJT(Fi{*fh=nV2yRF+`k0sc%luRGrluE1)wuKN8' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
